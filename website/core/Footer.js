/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    return `${baseUrl}${docsPart}${langPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Resourcen</h5>
            <a href="https://vcs.etrap.eu/masterarbeit-kkiefer/datengrundlage" target="_blank">
              Datengrundlage
            </a>
            <a href="https://vcs.etrap.eu/masterarbeit-kkiefer/skripte" target="_blank">
              Skripte
            </a>
          </div>
          <div>
            <h5>Kontakt</h5>
            <a
              href="https://twitter.com/nadelbaeumchen"
              target="_blank">
              Twitter
            </a>
          </div>
          <div>
            <h5>Über diese Seite</h5>
            <a href="https://gitlab.com/pages/docusaurus" target="_blank">Built by GitLab Pages</a>
            <a href="https://github.com/facebook/docusaurus" target="_blank">Docusaurus on GitHub</a>
            <a
              className="github-button"
              href={this.props.config.repoUrl}
              data-icon="octicon-star"
              data-count-href="/facebook/docusaurus/stargazers"
              data-show-count="true"
              data-count-aria-label="# stargazers on GitHub"
              aria-label="Star this project on GitHub">
              Star
            </a>
            {this.props.config.twitterUsername && (
              <div className="social">
                <a
                  href={`https://twitter.com/${this.props.config.twitterUsername}`}
                  className="twitter-follow-button">
                  Follow @{this.props.config.twitterUsername}
                </a>
              </div>
            )}
            {this.props.config.facebookAppId && (
              <div className="social">
                <div
                  className="fb-like"
                  data-href={this.props.config.url}
                  data-colorscheme="dark"
                  data-layout="standard"
                  data-share="true"
                  data-width="225"
                  data-show-faces="false"
                />
              </div>
            )}
          </div>
        </section>
        <section className="copyright">Die Masterarbeit wurde im Rahmen des Studiengangs <a href="https://www.digitale-methodik.uni-mainz.de/">Digitale Methodik in Kultur- und Geisteswissenschaften</a> an der Hochschule Mainz sowie der Johannes Gutenberg Universität angefertigt.</section>
      </footer>
    );
  }
}

module.exports = Footer;
