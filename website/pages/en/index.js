/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle">
        {siteConfig.title}
        <small>{siteConfig.tagline}</small>
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io" target="_blank">GitLab Repository</Button>
            <Button href="https://nadelbaum.gitlab.io/img/poster.pdf" target="_blank">Poster</Button>
          </PromoSection>
        </div>
        <br/>
        <div className="inner">
          <PromoSection>
          <Button href="#frage">Forschungsfrage</Button>
          <Button href="#try">Datengrundlage</Button>
          <Button href="#methode">Methode</Button>
          <Button href="#ergebnisse">Ergebnisse</Button> 
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

const FeatureCallout = null;

    const TryOut = () => (
      <Block id="try">
        {[
          {
            content:
              'Die Bewertungen für das Disneyland Paris stammen von der offiziellen Facebookseite des Freizeitparks und sind auf kaggle.com verfügbar. Der Datensatz besteht aus 300.000 Rezensionen verschiedener Sprachen von denen 533 auf deutsch und 2.394 auf englisch verfasst wurden. Die Werbetexte stammen aus der „Allgemeine Information“-Rubrik der Disneyland Paris-Homepage.<br><br>Die Rezensionen des Gesellschaftspiels Catan stammen von der Online-Community BoardGameGeek.com und sind ebenfalls auf kaggle.com einsehbar. Es liegen 59 deutsche und 12.522 englische Bewertungen vor. Die Werbetexte stammen vom Verlag des Spiels.<br><br>Für die Easybreath Maske werden die englischen und deutschen Bewertungen im Onlineshop von Decathlon gespeichert – es liegen 2.629 deutsche und 596 englische Rezensionen vor. Der Werbetext wird der Produktseite auf decathlon.de und decathlon.co.uk entnommen.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/rezension.svg`,
            imageAlign: 'left',
            title: 'Datengrundlage',
          },
        ]}
      </Block>
    );

    const Description = () => (
      <Block background="light" id ="methode">
        {[
          {
            content:
              'Zunächst erfolgt die Auslese von Product Features mit Hilfe der erstellten NomAd-Methode, anschließend Sentiment Analysis jedes Features. Als potenzielles Product Feature kommen Nomen, Adjektive und Bigrams (z.B. "freundliches Personal" oder "gateway game") in Frage. Ein Feature liegt vor, wenn ein Ausdruck in mehr als 1% aller Sätze im Korpus genannt wird.<br><br>Nach dem Einlesen der Rezensionen findet eine Suche nach häufig genannten Bigrams statt. Anschließend werden alle Nomen gespeichert, welche die 1%-Hürde erreichen. Anhand der Bigramliste werden alle Nomen überprüft, die Bestandteil eines Bigrams sind. Dabei wird die Häufigkeit des Bigrams von der Häufigkeit des Nomens subtrahiert - erreicht das Nomen alleinstehend die 1%-Hürde nicht, handelt es sich sehr wahrscheinlich nicht um ein Feature und es wird gestrichen. Ein Beispiel:<br> In 100 von 200 Sätzen fällt das Bigram "board game", das Nomen "board" wird im gesamten Korpus 201 mal gezählt. Nach der Subtraktion der Häufigkeit des Bigrams erreicht "board" die 1%-Hürde nicht mehr und wird aus der Liste der Product Features entfernt.<br><br>Da durch Adjektive Product Features häufig implizit angesprochen werden (z.B. "Ich finde es sehr teuer."), sind diese auch als potenzielle Features zu untersuchen. Erreicht ein Adjektiv die vorgeschriebene Mindestanzahl an Nennungen, wird es ebenfalls als Product Feature gespeichert.<br><br>Sind alle Features ausgelesen, erfolgt Sentiment Analysis mit Hilfe des textblob packages. Dabei handelt es sich um eine wörterbuchbasierte Methode. Jeder Satz, der (mindestens) ein Feature enthält, wird gespeichert und hinsichtlich seiner Polarität untersucht. Die Ergebnisse werden je nach Wert in die Kategorie "(sehr) positiv", "neutral" oder "(sehr) negativ" eingeordnet. Anschließend wird der durchschnittliche Polaritätswert jedes Features ermittelt und die Ergebnisse ausgewertet.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/methode.svg`,
            imageAlign: 'right',
            title: 'Methode',
          },
        ]}
      </Block>
    );

    const LearnHow = () => (
      <Block background="light" id = "frage">
        {[
          {
            content:
              'Aktuelle Studien zeigen, dass Online-Reviews ein einflussreiches Hilfsmittel zum Treffen von Kaufentscheidungen sind. Mehr als jeder zweite Nutzer von Online-Shops greift auf Produktbewertungen zurück wenn es darum geht, ein Produkt auszuwählen. Doch auch für die Anbieter sind Bewertungen ihrer Kunden eine wertvolle Quelle zur Qualitätskontrolle des eigenen Angebots.<br><br>Für einen Soll-Ist-Abgleich von Werbetext und Kundenrezension wurde eine Methode zur Auslese und Klassifizierung von Produktmerkmalen („Product Features“) erstellt. Konkret werden Werbetext und Bewertungen für das Disneyland Paris, das Gesellschaftsspiel Catan und die Easybreath Schnorchelmaske in Englisch und Deutsch untersucht, um zu demonstrieren, dass die Methode mit verschiedenen Sprachen und Produktdomänen umgehen kann.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/frage.svg`,
            imageAlign: 'right',
            title: 'Forschungsfrage',
          },
        ]}
      </Block>
    );
  
const Ergebnisse = () => (
      <Block id="ergebnisse" >
        {[
          {
            content:
              'Die Ergebnisse der Produktanalysen zeigen, dass es Schwächen bezüglich der gewählten Sentiment-Analysis-Methode gibt; so werden manche Wörter aufgrund des genutzten Sentimentwörterbuchs in falsche Sentimentkategorien eingeordnet. Ein Beispiel ist das Feature "game" im Catan-Korpus: für die Textblob-Methode steht das Wort für etwas Negatives; die Polarität liegt bei -0,4 - somit sind die erhaltenen Werte in diesem Korpus durchschnittlich negativer, da jede Nennung von "game" den Satz kontaminiert.<br><br>Die erstellte NomAd-Methode erzielt die untenstehenden Ergebnisse. <br><table><tr><th>Sprache</th><th>Precision</th><th>Recall</th></tr><tr><td>Deutsch</td><td>0,63</td><td>0,81</td></tr><tr><td>Englisch</td><td>0,52</td><td>0,94</td></tr></table><br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/ultimatearm" title="ultimatearm">ultimatearm</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/befriedigung.svg`,
            imageAlign: 'left',
            title: 'Ergebnisse',
          },
        ]}
      </Block>
    );
  
  const Disney = () => (
      <Block id="disney" background="light">
        {[
          {
            content:
              'Die Gäste sind insgesamt mit ihrem Besuch (sehr) zufrieden. Vor allem das freundliche Personal wird als sehr gut empfunden. Deutschsprachige Besucher bewerten das abendliche Feuerwerk sehr positiv, dieses wird nicht im Werbetext genannt, könnte jedoch aufgrund der positiven Rezeption darin aufgenommen werden.<br><br>Negative Aspekte sind in beiden Sprachen die Preise sowie geschlossene Attraktionen. Des Weiteren kann anhand der vorliegenden Bewertungen nicht abgebildet werden, dass die gewünschte Zielgruppe „Familie mit Kleinkind“ auch die Hauptgruppe der Besucher ausmacht, da entsprechende Signalwörter nur wenig bis gar nicht vorhanden sind.<br><br>Um die Anzahl an Besuchern zu steigern, wurden die „Walt Disney Studios“ und die Attraktion „Ratatouille“ gebaut. Der Soll-Ist-Abgleich zeigt jedoch, dass diese Product Features nur sehr selten in den Bewertungen genannt werden (WDS 16 mal, Ratatouille 32 mal). Die Bewertungen lassen somit keinen Schluss darüber zu, ob die Strategie hinter den Investitionen erfolgreich ist.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/disney-schloss.svg`,
            imageAlign: 'right',
            title: 'Disneyland Paris',
          },
        ]}
      </Block>
    );

  const Catan = () => (
      <Block id="catan" >
        {[
          {
            content:
              'Die Untersuchung zeigt, dass sich das Spiel als Klassiker etabliert hat und die verfügbaren Erweiterungen des Grundspiels gut angenommen werden, da diese in beiden Sprachen sehr häufig genannt werden. Die Regeln des Spiels werden in 60% der Fälle als (sehr) positiv bewertet. Lediglich das Zufallselement durch die Nutzung von Würfeln empfinden über 50% der englischen Spieler als negativ.<br><br>Die Werbetexte beider Sprachen unterscheiden sich voneinander: während der deutsche Text die niedrige Einstiegshürde des Familienspiels unterstreicht, ordnet die englischsprachige Werbung das Spiel in die Kategorie „war game“ ein; leichtes Erlernen des Spiels ist nicht Teil des Werbetextes. Trotzdem wird Catan im englischsprachigen Raum häufig als Familienspiel oder „gateway game“ bezeichnet. Dabei handelt es sich um ein Spiel, das einfach zu erlernen ist und Nicht-Spielern das „Tor zur Spielewelt“ öffnet. Beide Features sind Indikatoren dafür, dass die englische Werbestrategie für das Produkt überdacht werden könnte.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/ddara" title="dDara">dDara</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/brettspiel.svg`,
            imageAlign: 'left',
            title: 'Catan',
          },
        ]}
      </Block>
    );

  const Easybreath = () => (
      <Block id="easybreath" background="light">
        {[
          {
            content:
              'Bei der Easybreath Maske zeigt die Analyse, dass im englischen Korpus kein Feature vorhanden ist, das einen negativen Sentimentwert erzielt. Insgesamt handelt es sich bei der Maske um ein fast „perfektes“ Produkt, da alle untersuchten Features in beiden Sprachen positiv bis sehr positiv klassiziert werden und die Anteile an negativen Äußerungen sehr gering ist. Ausnahme ist hier die Dichtigkeit des Produkts: ein Merkmal, das in beiden Sprachen den höchsten Anteil an negativen Anmerkungen hat. <br><br>Vor allem bei der durch die neuartige Bauweise der Maske mögliche Rundumsicht handelt es sich um ein überdurchschnittlich gut bewertetes Feature (deutsch: 77% positiv, englisch 92% positiv).<br><br>Die Analyse zeigt, dass anhand der Bewertungen Familien und Anfänger als Hauptnutzergruppe des Produkts ausgemacht werden können. Eine Anpassung des Werbetextes in diese Richtung könnte die Verkaufszahlen steigern.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/tauchermaske.svg`,
            imageAlign: 'right',
            title: 'Easybreath Schnorchelmaske',
          },
        ]}
      </Block>
    );

    const Exkurs = () => (
      <Block id="exkurs">
        {[
          {
            content:
              'Im Rahmen der Arbeit fand eine intensive Auseinandersetzung mit Rezensionen verschiedenster Produkte statt. Dies übersteigt die drei hier genannten Produkte Disneyland Paris, Catan und Schnorchelmaske. Bei der Erstellung eines Goldstanrards entstand die These, dass sehr gute Rezensionen am kürzesten sind, während schlechte Bewertungen länger ausfallen, da zu erwarten ist, dass Kunden den Grund ihrer Unzufriedenheit mitteilen möchten.<br><br>Um dies zu untersuchen, werden einerseits die Bewertungen aus dem erstellten Goldstandard, andererseits die Bewertungen von Produkten mit vielen Kommentaren genauer untersucht.<br><br>Für alle Produkte ist die Anzahl kurzer Bewertungen deutlich höher als die von langen Bewertungen. In der getroffenen Auswahl sind nur 25% aller Bewertungen länger als 100 Wörter. Für die Untersuchung werden sowohl Produkte mit über tausend als auch mit nur fünfzig Bewertungen einbezogen.<br><br>Reviewlängen bis 20 Wörter machen mindestens 40% aller Bewertungen aus. Die Verteilung der Bewertungslängen unterscheiden sich innerhalb einer Domäne voneinander, so gestaltet sich bspw. die Verteilung der <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verteilung%20Bewertungsl%C3%A4ngen/easybreath_de.png" target="_blank">Tauchermasken-Reviews in Deutsch</a> anders als die <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verteilung%20Bewertungsl%C3%A4ngen/easybreath_en.png" target="_blank">im Englischen</a>.<br><br>Bei der Betrachtung der vergebenen Noten für die Produkte zeigt sich, dass deren Verteilung innerhalb der Domäne unabhängig von der Sprache ist; z.B. folgt das Verteilungsmuster von Catan sowohl <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verteilung%20Ratings/catan_ratings_de.png" target="_blank">in Deutsch</a> als auch <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verteilung%20Ratings/catan_ratings_en.png" target="_blank">in Englisch</a> immer dem gleichen Schema: 4-3-2-5-1.<br><br>Die Note 5, also „sehr gut“ wird für <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/tree/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verteilung%20Ratings" target="_blank">Produkte aus der Kategorie Sportartikel sowie das Disneyland</a> in mehr als 50% der Gesamtbewertungen vergeben. Dieser Prozentsatz zeigt sich im Übrigen auch bei neun Produkten des Goldstandards.<br><br>Um die Länge der Bewertung mit der vergebenen Note ins Verhältnis zu setzen, wird zunächst die durchschnittliche Länge pro Note berechnet und diese Ergebnisse anschließend <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/tree/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verh%C3%A4ltnis%20L%C3%A4nge-Rating" target="_blank">graphisch dargestellt</a>.<br>Für alle Produkte ist ein Trend erkennbar, der die These bestätigt. Die Ergebnisse des <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verh%C3%A4ltnis%20L%C3%A4nge-Rating/disneyland_de_en.png" target="_blank">Disneyland Paris</a> sind hierbei am eindeutigsten. Sie zeigen, dass Bewertungen der Note 2 im Durchschnitt am längsten sind. Dies ist ebenfalls bei sechs anderen Verhältniskurven der Analyseauswahl, sowie sieben von zehn Produkten des Goldstandards, der Fall.<br><br>Wie im Disneyland-Beispiel zu sehen, sind Bewertungen der Note 1 und 2 durchschnittlich länger, während der Bewertungen der Noten 4 und 5 kürzer sind - dies gilt ebenso für <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verh%C3%A4ltnis%20L%C3%A4nge-Rating/easybreath_de_en.png" target="_blank">die Schnorchelmaske</a> und <a href="https://gitlab.com/nadelbaum/nadelbaum.gitlab.io/-/raw/master/Daten/Exkurs-Verh%C3%A4ltnis-L%C3%A4nge-Note/Verh%C3%A4ltnis%20L%C3%A4nge-Rating/catan_de_en.png" target="_blank">die englischen Bewertungen von Catan</a>.<br><br>Die Verhältnisse von durchschnittlicher Ratinglänge und Bewertungsnote im Goldstandard bestätigen in sieben von zehn Fällen die These, dass mit zunehmender Länge einer Rezension ebenso die Wahrscheinlichkeit steigt, dass eine negative Bewertung vorliegt.<br><small style="font-size: 10px;">Icon erstellt von <a href="https://www.flaticon.com/de/autoren/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></small>',
            image: `${baseUrl}img/bericht_diagramm.svg`,
            imageAlign: 'left',
            title: 'Exkurs Bewertungslänge',
          },
        ]}
      </Block>
    );
      const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter(user => user.pinned)
        .map(user => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = page => baseUrl + (language ? `${language}/` : '') + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>Who is Using This?</h2>
          <p>This project is used by all these people</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button" href={pageUrl('users.html')}>
              More {siteConfig.title} Users
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <LearnHow />
          <TryOut />
          <Description />
          <Ergebnisse />
          <Disney />
          <Catan />
          <Easybreath />
          <Exkurs />
        </div>
      </div>
    );
  }
}

module.exports = Index;
