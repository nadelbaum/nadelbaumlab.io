import scrapy
from items import ProductReview

class ProductSpider(scrapy.Spider):
    name = 'productItems'

    def start_requests(self):

        urls = [
            'https://www.decathlon.de/r/schnorchelmaske-easybreath/_/R-p-1616?mc=8401951&c=GR%C3%9CN_T%C3%9CRKIS'   # gewünschtes Produkt auf decathlon.de suchen, Produktansicht --> "Alle Bewertungen" --> Link hier einfügen
            ]
        return [scrapy.Request(url=url, callback=self.parse_items) for url in urls]

    def parse_items(self, response):
        for rev in response.css(".review__item"):
            yield {
                'product': response.css('p.title::text').get(),
                'user': rev.css('h4.review__author-name::text').get(),
                'rating': rev.css('strong.review__rating::text').get(),
                'title': rev.css('.review__title::text').get(),
                'text': rev.css('p.review__text::text').getall(),
                'pos': rev.css('.dkti-arrow1-up + span::text').get(),
                'neg': rev.css('.dkti-arrow1-down + span::text').get(),
                'date': rev.css('.review__date::text').get(),
            }
            

        next_page = response.css(".active + li a::attr('href')").get()
        if next_page is not None:
            yield response.follow(next_page, self.parse_items)