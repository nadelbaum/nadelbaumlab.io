import spacy
import os
import csv
import nltk
from nltk.tokenize import sent_tokenize
from fuzzywuzzy import fuzz

class PotentialFeature(object):
    def __init__(self, expression):
        self.expression = expression

class ProductFeature(object):
    def __init__(self, name, count):
        self.name = name
        self.count = count

nlp = spacy.load("de_core_news_md")   # Lade spacy-Modell für POS

# Reviews einlesen, Zeilen 24-30 je nach Produkt anpassen (Decathlon alle aktivieren, Disney row[1], Catan [2])
with open('DE_Disney_GS.csv', 'r', errors='ignore', newline='') as csvinput:   # , encoding='utf-8' bei Bedarf einfügen
    reader = csv.reader(csvinput)
    next(reader)
    all = []
    for row in reader:
        text = row[1] + '. '#+ row[4] + '. '
        # if row [5]:
        #     text = text + row[5]+ '. '
        # if row[6]:
        #     text = text + row[6]+'. '
        # text = text.lower()               # Optionales Kleinschreiben aller Worte
        all.append(text)
    all = ' '.join(all)
sentences = sent_tokenize(all)

print('working')
noun_list = []
potential_list = []

# Suche Nomen und Nomengruppen
for sent in sentences:
    sentence_count = sentence_count+1
    doc = nlp(str(sent)) #<-- !!!
    noun_chunks = doc.noun_chunks
    for token in doc:
        if token.pos_ == 'NOUN' and token.text not in noun_list:
            noun_list.append(token.text)
    for chunk in noun_chunks:
        l = len(chunk)
        i = 0
        for word in chunk:
            if word.pos_ == 'NOUN':
                i = i+1
        if i == l and ' ' in chunk.text:
            potential_list.append(PotentialFeature(chunk.text))
for word in noun_list:
    potential_list.append(PotentialFeature(word))

# Berechnung 1% Hürde
sentence_count = 0
min_anzahl = sentence_count / 100

# Zählung Feature-Häufigkeit, Speicherung Features über 1%-Hürde
frequent_feature = []
for word in potential_list:
    word_count = 0
    check_l = nlp(word.expression)
    if len(check_l) == 1:
        for sent in sentences:
            docs = nlp(sent)
            for token in docs:
                if token.text == word.expression:
                    word_count = word_count + 1
    if len(check_l) > 1:
        for sent in sentences:
            if word.expression in sent:
                word_count = word_count+1
    if word_count >= min_anzahl: 
        frequent_feature.append(ProductFeature(word.expression,word_count))

# Fuzzy Matching + Löschen Dopplungen
for f in frequent_feature:
    for other in frequent_feature:
        if fuzz.partial_ratio(f.name, other.name) == 100:
            print (f.name, other.name)
            if len(f.name) < len(other.name) or len(f.name) > len(other.name):
                dif = len(other.name) - len(f.name)
                if abs(dif) <= 2:
                    sum = int(f.count) + int(other.count)
                    f.count = sum
                    frequent_feature.remove(other)
                    print('doppeltes feature entfernt')

# Ausgabe Features mit Häufigkeit in Konsole
for f in frequent_feature:
    print(f.name, f.count)
