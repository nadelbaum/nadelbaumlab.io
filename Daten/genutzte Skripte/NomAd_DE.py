import spacy
import os
import csv
import nltk
from nltk.tokenize import sent_tokenize
from fuzzywuzzy import fuzz
from nltk.collocations import BigramCollocationFinder
from nltk.corpus import stopwords
from textblob_de import TextBlobDE

class ProductFeature(object):
    def __init__(self, name, count):
        self.name = name
        self.count = count

# definiere Stopwords und ignorierte Satzzeichen
ignored_words = nltk.corpus.stopwords.words('german')
ignored_words.append('.')
ignored_words.append('?')
ignored_words.append('!')
ignored_words.append(',')
ignored_words.append('...')
ignored_words.append("'")
ignored_words.append(':')
ignored_words.append(';')
ignored_words.append('&')
ignored_words.append('=')
ignored_words.append('[')
ignored_words.append(']')

# definiere Blacklist Adjektive
ignored_adj = ['eigentlich', 'extrem', 'ganz', 'ganze', 'ganzer', 'ganzes', 'ganzem', 'ganzen', 'immer', 
    'total', 'wirklich', 'mehr', 'mehreren', 'mehrerem', 'mehrere', 'gleich', 'schon', 'echt', 'absolut']

nlp = spacy.load("de_core_news_md")   # Lade spacy-POS-Modell
final_list = []
all = []
sentence_count = 0

# Einlesen von Bewertungen, Zeilen 45-49 je nach Produkt anpassen (Decathlon alle aktivieren, Disney row[1], Catan [2])
with open('disney_de.csv', 'r', errors='ignore', newline='') as csvinput:
    reader = csv.reader(csvinput)
    next(reader)
    for row in reader:
        text = row[1] + '. '#+ row[4] + '. '
        # if row [5]:
        #     text = text + row[5]+ '. '
        # if row[6]:
        #     text = text + row[6]+'. '
        # text = text.lower()               # Optionales Kleinschreiben aller Worte
        all.append(text)
    all = ' '.join(all)

# Verarbeitung der Bewertungen zu Textblob-Instanz + Aufteilung in Sätze
blob = TextBlobDE(all)
tokens = nltk.word_tokenize(all)
review_text = nltk.Text(tokens)
sentences = sent_tokenize(all)

# Ermitteln der 1% Hürde
for sent in sentences:
    sentence_count = sentence_count+1
min_amount = (sentence_count * 1) / 100
if min_amount <= 1:
    min_amount = 2
print(min_amount)

#### START BIGRAMS ####
pf_list = []
features = []
bigram_list = []

bigram_measures = nltk.collocations.BigramAssocMeasures()
finder = BigramCollocationFinder.from_words(review_text, window_size= 2 )
finder.apply_word_filter(lambda w: w.lower() in ignored_words)
finder.apply_freq_filter(2)         # Bigram muss mindestens zweimal im Korpus vorkommen
print('Starte Bigram-Suche')

# Speichern der Resultate und ausfiltern von Dopplungen
result = finder.nbest(bigram_measures.pmi, 100000)
for t in result:
    if t not in pf_list:
        pf_list.append(t)

# Erstellen einer Liste mit allen Bigrams, incl. Häufigkeit
for f in pf_list:
    word_count = 0
    for sent in sentences:
        feature = f[0] + ' ' + f[1]
        if feature in sent or feature.lower() in sent.lower():
            word_count = word_count + 1
    features.append(ProductFeature(feature,word_count))

# Fuzzy Matching, zusammenzuführende Bigrams dürfen nur zwei Buchstaben länger sein als das zu prüfende Bigram
for f in features:
    for other in features:
        if f.name == other.name:
            pass
        else:
            if fuzz.partial_ratio(f.name, other.name) == 100:
                if len(f.name) < len(other.name):
                    dif = len(other.name) - len(f.name)
                    if abs(dif) <= 2:
                        features.remove(other)
                        f.count = f.count + other.count

# Speichern aller Bigrams, welche die 1% Hürde erreichen
for f in features:
    if f.count >= min_amount:
        final_list.append(f)
        bigram_list.append(f)
        print('Speichere Bigram')
print('----Ende Bigramsuche----')
#### ENDE BIGRAMS ####

#### START SUCHE NOMEN ADJ####
noun_list = []
adj_list = []
mono_list = []

# Speichern aller Nomen
nouns = [word for (word, pos) in blob.tags if(pos[:2] == 'NN') or(pos[:2] == 'NNS')]
for word in nouns:
    if word not in noun_list and word not in ignored_words:
        noun_list.append(word)

for word in noun_list:
    word_count = 0
    test = nlp(str(word))       # zweite Überprüfung auf Nomen mit spacy-Modell
    for token in test:
        if token.pos_ == 'NOUN':
            for sent in sentences:
                docs = nltk.word_tokenize(sent)
                for token in docs:
                    if token == word:
                        word_count = word_count + 1
    if word_count >= min_amount:
        print('Speichere Nomen')
        mono_list.append(ProductFeature(word,word_count))

# Speichern aller Adjektive
adjectives = [word for (word, pos) in blob.tags if(pos[:2] == 'JJ')]
for word in adjectives:
    if word not in adj_list and word not in ignored_adj and word not in ignored_words:
        adj_list.append(word)

for word in adj_list:
    word_count = 0
    for sent in sentences:
        s = nltk.word_tokenize(sent)
        for token in s:
            if token == word:
                word_count = word_count + 1
    if word_count >= min_amount:
        print('Speichere Adjektiv')
        mono_list.append(ProductFeature(word,word_count))

# Fuzzy matching, zusammenzuführende Features dürfen nur zwei Buchstaben länger sein als das zu prüfende Feature
for f in mono_list:
    print('Fuzzy matching...')
    for other in mono_list:
        if fuzz.partial_ratio(f.name, other.name) == 100:
            if len(f.name) < len(other.name) or len(f.name) > len(other.name):
                dif = len(other.name) - len(f.name)
                if abs(dif) <= 2:
                    sum = int(f.count) + int(other.count)
                    f.count = sum
                    mono_list.remove(other)

# Pruning mit Hilfe der Bigrams
remove_list = []
for word in mono_list:
    print('Pruning...')
    for feature in bigram_list:
        f_words = feature.name.split()
        if word.name == f_words[0] or word.name == f_words[1]:
            diff = word.count - feature.count
            word.count = diff
            if diff < min_amount:
                remove_list.append(ProductFeature(word.name, word.count))
for removed_word in remove_list:
    for word in mono_list:
        if removed_word.name == word.name:
            print (word.name)
            mono_list.remove(word)

# Speichern der Features
for f in mono_list:
    final_list.append(f)
#### ENDE NOMEN ADJ####

# Löschen von Dopplungen
position = 0
for f in final_list:
    other_position = 0
    for other in final_list:
        if (f.name == other.name and position != other_position) or (f.name == other.name.lower() and position != other_position):
            final_list.remove(other)
        other_position = other_position+1
    position = position+1

# Ausgabe der Features mit Häufigkeit in der Konsole
for f in final_list:
    print (f.name + ', '+ str(f.count))