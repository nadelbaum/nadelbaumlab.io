import scrapy

class ProductSpider(scrapy.Spider):
    name = 'products_reviews'

    def start_requests(self):
        urls = [
            'https://www.decathlon.de/sports'
            ]
        return [scrapy.Request(url=url, callback=self.parse_categories) for url in urls]

    def parse_categories(self, response):
        for li in response.css(".hs-list__link"):
            url =  li.css("a::attr('href')").get()
            yield response.follow(url, self.parse_sport)

    def parse_sport(self, response):
        for kat in response.css(".sub-category"):
            url = kat.css(".sub-category__title > a::attr('href')").get()
            yield response.follow(url, self.parse_product)
    
    def parse_product(self, response):
        for prod in response.css("article"):
            url = prod.css(".dkt-product-slider a::attr('href')").get()
            prod_url = url.split('/p/')[-1]
            prod_url = '/r/'+prod_url
            yield response.follow(prod_url, self.parse_items)
        
        next_page = response.css("ul.pagination li:last-child a::attr('href')").get()
        if next_page is not None:
            (yield response.follow(next_page, self.parse_product))
    
    def parse_items(self, response):
        for rev in response.css(".review__item"):
            yield {
                'product': response.css('p.title::text').get(),
                'user': rev.css('h4.review__author-name::text').get(),
                'rating': rev.css('strong.review__rating::text').get(),
                'title': rev.css('.review__title::text').get(),
                'text': rev.css('p.review__text::text').getall(),
                'pos': rev.css('.dkti-arrow1-up + span::text').get(),
                'neg': rev.css('.dkti-arrow1-down + span::text').get(),
                'fit': rev.css('.dkti-size-guide + span::text').get(),
                'date': rev.css('.review__date::text').get(),
                'link': response.url
            }
        
        next_page = response.css(".active + li a::attr('href')").get()
        if next_page is not None:
            yield response.follow(next_page, self.parse_items)