import os
import csv

features = []

 # Einlesen der Features für die Rückwärtssuche
with open('Features_Rückwärtssuche.csv', 'r', errors='ignore', newline='') as csvinput_pf:
    pf_reader = csv.reader(csvinput_pf)
    next(pf_reader)
    for row in pf_reader:
        if row[0]:          # row-Nummer nach Produkt anpassen
            text = row[0]
            features.append(text.lower())

# Zählen aller Sentimentkategorien sowie Häufigkeit und Durchschnitts-Polarität pro Feature
for f in features:
    very_pos = 0
    pos = 0
    n = 0
    neg = 0
    very_neg = 0
    polarity = 0
    counter = 0
    try:
        # Einlesen der Sentiment-Ergebnisse
        with open('Disneyland_DE_Sentences.csv', 'r', errors='ignore', newline='') as csvinput: #, encoding='utf-8'  bei Bedarf einfügen
            reader = csv.reader(csvinput)
            next(reader)
            for row in reader:
                if row[0] == f:
                    counter += 1
                    if row[2] == "stark negativ":
                        very_neg += 1
                    elif row[2] == "stark positiv":
                        very_pos +=1
                    elif row[2] == "negativ":
                        neg += 1
                    elif row[2] == "positiv":
                        pos += 1
                    elif row[2] == "neutral":
                        n += 1
                    else:
                        print (' ۜ\(סּںסּَ` )/ۜ')
                    polarity = polarity + float(row[3])
    except:
        print('Feature nicht gefunden.')
    if counter == 0:
        durchschnitt = 0
    else:
        durchschnitt = polarity / counter

    # Ausgabe von Feature , Kategorie sehr positiv, Kategorie positiv, Kategorie neutral, Kategorie negativ, Kategorie sehr negativ, Durchschnitt, Häufigkeit
    print (f+', '+str(very_pos)+', '+str(pos)+', '+str(n)+', '+str(neg)+', '+str(very_neg)+', '+str(durchschnitt)+', '+str(counter))




