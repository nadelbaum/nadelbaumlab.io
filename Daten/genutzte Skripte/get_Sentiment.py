import os
import csv
import nltk
from nltk.tokenize import sent_tokenize
from textblob_de import TextBlobDE
from textblob import TextBlob
import pandas as pd
from fuzzywuzzy import fuzz

def get_category(sentiment):
    if sentiment <= -0.5:
        cat = 'stark negativ'
    if sentiment > -0.5 and sentiment < 0:
        cat = 'negativ'
    if sentiment == 0:
        cat = 'neutral'
    if sentiment >= 0.5:
        cat = 'stark positiv'
    if sentiment > 0 and sentiment < 0.5:
        cat = 'positiv'
    return cat

# Definition Dictionary Review
reviews = {
        "Feature":[],
        "Satz": [],
        "Kategorie":[],
        "Wert": []
        }

features = []
all_sentences = []

# Einlesen Product Features eines Produkts
with open('Features_Disney_Catan_Easybreath', 'r', errors='ignore', newline='') as csvinput_pf:
    pf_reader = csv.reader(csvinput_pf)
    next(pf_reader)
    for row in pf_reader:
        if row[0]:                  # row-Nummer nach Produkt anpassen
            text = row[0]
            features.append(text.lower())

# Einlesen Produktbewertungen, Zeilen 48-52 je nach Produkt anpassen (Decathlon deutsch alle aktivieren Decathlon englisch row[3] + row[4], Disney row[1], Catan [2])
with open('disney_de.csv', 'r', errors='ignore', newline='') as csvinput:   #, encoding='utf-8' bei Bedarf einfügen
    reader = csv.reader(csvinput)
    next(reader)
    for row in reader:
        text = row[1] + '. ' #+ row[4] + '. '
        # if row [5]:
        #     text = text +row[5]+ '. '
        # if row[6]:
        #     text = text+row[6] + '. '
        text = text.lower()
        all_sentences.append(text)
    all_sentences = ' '.join(all_sentences)

# Aufteilen der Sätze
sentences = sent_tokenize(all_sentences)

# Suche nach allen Sätzen mit Feature
for f in features:
    token = nltk.word_tokenize(f)
    if len(token)> 1:               # Bigram
        for sent in sentences:
            if f in sent:
                blob = TextBlobDE(sent)
                polarity_score = blob.sentiment.polarity
                cat = get_category(polarity_score)
                reviews["Feature"].append(f)
                reviews["Satz"].append(sent)
                reviews["Kategorie"].append(cat)
                reviews["Wert"].append(polarity_score)
    else:                           # Unigram
        for sent in sentences:
            tokens = nltk.word_tokenize(sent)
            for t in tokens:
                if t == f:
                    blob = TextBlobDE(sent)
                    polarity_score = blob.sentiment.polarity
                    cat = get_category(polarity_score)
                    reviews["Feature"].append(f)
                    reviews["Satz"].append(sent)
                    reviews["Kategorie"].append(cat)
                    reviews["Wert"].append(polarity_score)

review_data = pd.DataFrame(reviews)
print(review_data)
review_data.to_csv('Disneyland_DE_Sentences.csv', index=False)