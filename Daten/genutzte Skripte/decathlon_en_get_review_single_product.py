import scrapy


class ProductEnglischSpider(scrapy.Spider):
    name = "enDecathlon"

    def start_requests(self):
        urls = [
            'https://www.decathlon.co.uk/en/reviewsNext?productId=%228401951%22&collaborator=0&numPage=1'   # über Product-ID gewünschtes Produkt angeben
        ]
        return [scrapy.Request(url=url, callback=self.parse_items) for url in urls]

    def parse_items(self, response):
        if response.css(".review-container"):
            for rev in response.css(".review-container"):
                if rev.css(".r-country::text").get() == "(United Kingdom)" or rev.css(".r-country::text").get() == "(Australia)" or rev.css(".r-country::text").get() == "(Hong Kong)" or rev.css(".r-country::text").get() == "(Canada)":      # Speichere alle Kommentare aus den gelisteten Ländern
                    url = response.url[41:48]
                    yield {
                        'product-id': url,
                        'user': rev.css('.r-user-firstname::text').get(),
                        'rating': rev.css('.r-note-resume::text').get(),
                        'title': rev.css('.r-title-resume::text').get(),
                        'text': rev.css('p.r-desc::text').getall(),
                        'pos': rev.css('.r-advantages + span::text').get(),
                        'neg': rev.css('.r-disadvantages + span::text').get(),
                        'date': rev.css('.r-date::text').get(),
                        'link': response.url
                    }
            old_url = response.url
            pagenr = old_url.split("numPage=")[-1]
            pagenr = int(pagenr)+1
            next_url = old_url.split("numPage=")[0]+"numPage="+str(pagenr)
            yield response.follow(next_url, self.parse_items)
