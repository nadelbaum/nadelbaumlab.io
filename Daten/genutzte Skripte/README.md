# Skripte


Die folgenden Packages wurden für die Masterarbeit unter Python 3.6.7. verwendet:

| Package            | Version |
|--------------------|---------|
| de-core-news-md    | 2.2.5   |
| en-core-web-lg     | 2.2.5   |
| fuzzywuzzy         | 0.18.0  |
| langdetect         | 1.0.7   |
| nltk               | 3.4.5   |
| pandas             | 1.0.3   |
| pip                | 20.1.1  |
| Scrapy             | 1.7.4   |
| spacy              | 2.2.3   |
| spacy-lookups-data | 0.2.0   |
| textblob           | 0.15.3  |
| textblob-de        | 0.4.3   |

Die verfügbaren Skripte und Dateien wurden im Rahmen der Arbeit wie folgt genutzt:

| Skript / Datei                         | Verwendung im Rahmen der Arbeit                                                                             |
|----------------------------------------|-------------------------------------------------------------------------------------------------------------|
| blacklist_deutsch                      | Liste aller zu ignorierenden Adjektive, deutsch                                                             |
| blacklist_englisch                     | Liste aller zu ignorierenden Adjektive, englisch                                                            |
| decathlon_Bewertungen_alle_Produkte    | Scrapen des kompletten deutschen Decathlon-Webshops um Produkte mit größter Anzahl an Bewertungen zu finden |
| decathlon_de_get_review_single_product | gezieltes Scrapen eines bestimmten Produktes, deutscher Shop                                                |
| decathlon_en_get_review_single_product | gezieltes Scrapen eines bestimmten Produktes, englischer Shop                                               |
| get_Sentiment                          | Sentiment Analysis mit Hilfe einer Product Feature Liste                                                    |
| Hu_Liu_Nachbau                         | Nachbau der Hu-Liu-Methode                                                                                  |
| NomAd_DE                               | Deutsche Version NomAd-Methode                                                                              |
| NomAd_EN                               | Englische Version NomAd-Methode                                                                             |
| Sentiment_summary                      | Zusammenfassen der Ergebnisse aus get_Sentiment.py                                                          |
